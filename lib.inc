section .text

%define TAB 0x9
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, rdi
    xor rdi, rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0
    .loop:
      xor rax, rax
    .count:
      cmp byte [rdi+rax], 0
      je .end
      inc rax
      jmp .count
    .end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov  rdx, rax
    pop rsi
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
; Принимает код символа и выводит его в stdout
print_char:
     push rdi
     mov  rax, 1
     mov  rdi, 1
     mov  rsi, rsp
     mov  rdx, 1
     syscall
     pop rax
     ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, 10
    mov r9, 0
   .div_loop:
        xor rdx, rdx
        div r8
        add rdx, '0'
        push rdx
        inc r9
        test rax, rax
        jne .div_loop

    .print_loop:
        pop rdi
        call print_char
        dec r9
        test r9, r9
        jne .print_loop
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r8, r8
    xor rax, rax
    .loop:
        mov r8b, byte[rsi+rax]
        cmp byte[rdi+rax], r8b
        jne .fail
        cmp byte[rdi+rax], 0
        je .ok
        inc rax
        jmp .loop
    .ok:
        mov rax, 1
        ret
    .fail:
        mov rax, 0
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
     mov  rax, 0         
     mov  rdi, 0      
     mov  rsi, rsp  
     mov  rdx, 1
     syscall
     pop rax
     ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r9, rsi
    xor r8, r8
    .loop:
        push r9
        push r8
        push rdi
        call read_char
        pop rdi
        pop r8
        pop r9

        cmp rax, ' '
        je .check
        cmp rax, TAB
        je .check
        cmp rax, '\n'
        je .check

        cmp rax, 0
        je .ok

        cmp r8, r9
        je .fail

        mov byte[rdi + r8], al
        inc r8
        jmp .loop

    .check:
        test r8, r8
        je .loop
    .ok:
        mov rax, rdi
        mov rdx, r8
        mov byte[rdi + r8], 0
        inc r8
        ret
    .fail:
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, 10

    .loop:
        xor r9, r9
        mov r9b, byte[rdi]

        cmp r9b, '0'
        jl .end
        cmp r9b, '9'
        jg .end

        inc rcx
        mul r8
        sub r9b, '0'
        add rax, r9
        inc rdi
        jmp .loop
    .end:
        mov rdx, rcx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp al, '-'
    je .sign
    jmp parse_uint
    .sign:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r8, r8
    .loop:
        mov r8b,  byte[rdi + rax]
        mov byte[rsi + rax], r8b
        inc rax
        cmp rax, rdx
        jge .fail
        cmp r8b, 0
        je .ok
        jmp .loop
    .ok:
        ret
    .fail:
        xor rax, rax
        ret
